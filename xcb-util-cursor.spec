Name:		xcb-util-cursor
Version:	0.1.5
Release:	1
Summary:	Cursor library on top of libxcb
License:	MIT
URL:		https://xcb.freedesktop.org
Source0:	https://www.x.org/releases/individual/xcb/%{name}-%{version}.tar.xz
BuildRequires:  gcc
BuildRequires:	pkgconfig(xcb) >= 1.4
BuildRequires:	pkgconfig(xcb-render)
BuildRequires:	pkgconfig(xcb-renderutil)
BuildRequires:	pkgconfig(xcb-image)


%description
XCB util-cursor module provides the following libraries:

  - cursor: port of libxcursor


%package 	devel
Summary:	Development and header files for xcb-util-cursos
Requires:	%{name}%{?_isa} = %{version}-%{release}

%description	devel
Development files for xcb-util-cursor.


%prep
%setup -q


%build
%configure --with-pic --disable-static --disable-silent-rules
%make_build


%check
%make_build check


%install
%make_install
%delete_la

%files
%doc README.md
%license COPYING
%{_libdir}/*.so.*


%files devel
%doc NEWS
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so
%{_includedir}/xcb/*.h


%changelog
* Sun Nov 24 2024 Funda Wang <fundawang@yeah.net> - 0.1.5-1
- update to 0.1.5

* Mon May 29 2023 wulei <wu_lei@hoperun.com> - 0.1.4-1
- Upgrade package to version 0.1.4

* Sat Jan 7 2023 yangbo <yangbo1@xfusion.com> - 0.1.3-3
- Build xz tarballs instead of bzip2

* Tue Nov 22 2022 caodongxia <caodongxia@h-partners.com> - 0.1.3-2
- Modify invalid source0

* Fri Aug 7 2020 weidong <weidong@uniontech.com> - 0.1.3-1
- Initial release for OpenEuler
